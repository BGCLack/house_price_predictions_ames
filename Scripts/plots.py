import pandas as pd
import numpy as np
import os

from matplotlib import pyplot as plt
import seaborn as sns

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DATA_DIR = os.path.join(BASE_DIR, 'Data')
OUTPUT_DIR = os.path.join(BASE_DIR, 'Output')
IMAGE_DIR = os.path.join(OUTPUT_DIR, 'Images')

plt.style.use('seaborn-darkgrid')

def pairplot(data):
    columns = ['YearBuilt', 'OverallQual', 'OverallCond', 'LotArea',
    'BedroomAbvGr', 'FullBath', 'HalfBath', 'GarageCars', 'PoolArea', 'Fireplaces',
    'YearRemodAdd', 'GrLivArea', 'TotRmsAbvGrd', 'TotalBsmtSF', 'SalePrice']

    X = data[columns]

    # scatterplots(X)

    sns.pairplot(data=X)
    plt.show()

def distplots(data, log_trans=False):
    columns = ['YearBuilt', 'OverallQual', 'OverallCond', 'LotArea',
    'BedroomAbvGr', 'FullBath', 'HalfBath', 'GarageCars', 'Fireplaces',
    'YearRemodAdd', 'GrLivArea', 'TotRmsAbvGrd', 'TotalBsmtSF', 'SalePrice']

    if log_trans == True:
        data['HasBsmt'] = pd.Series(len(data['TotalBsmtSF']), index=data.index)
        data['HasBsmt'] = 0
        data.loc[data['TotalBsmtSF']>0,'HasBsmt'] = 1

        data.loc[data['HasBsmt']==1,'TotalBsmtSF'] = np.log(data['TotalBsmtSF'])
        data['SalePrice'] = np.log(data['SalePrice'])
        data['GrLivArea'] = np.log(data['GrLivArea'])
        data['LotArea'] = np.log(data['LotArea'])
        data['TotRmsAbvGrd'] = np.log(data['TotRmsAbvGrd'])

    fig, ax = plt.subplots(nrows=5, ncols=3, figsize=(12,15))
    for idx, col in enumerate(columns):
        # print(col)
        # print(idx % 3)
        # print(int(idx/3))
        # print('ax[{},{}]'.format(int(idx/3), idx % 3))

        sns.distplot(data[col], ax=ax[int(idx/3),idx % 3], rug=True)
    fig.tight_layout()
    plt.show()
    # plt.savefig(os.path.join(IMAGE_DIR, "distplots_numeric_afe.png"), dpi=500)

def scatterplots(data):
    fig, (ax1, ax2) = plt.subplots(nrows=1, ncols=2, figsize=(12,6))
    sns.scatterplot(x='YearBuilt', y='SalePrice', ax=ax1, data=data)

    if export == True:
        plt.savefig(os.path.join(IMAGE_DIR, filename))
    else:
        plt.show()

def corr_matrix(data):
    columns = ['YearBuilt', 'OverallQual', 'OverallCond', 'LotArea',
    'BedroomAbvGr', 'FullBath', 'HalfBath', 'GarageCars', 'PoolArea', 'Fireplaces',
    'YearRemodAdd', 'GrLivArea', 'TotRmsAbvGrd', 'TotalBsmtSF', 'SalePrice']

    correl = data[columns].corr()

    fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(12,12))
    ax.set_title("Correlation matrix of numerical predictors")
    sns.heatmap(data=correl, annot=True, cmap="YlGnBu", square=True, ax=ax)
    # plt.show()
    plt.savefig(os.path.join(IMAGE_DIR, "corr_matrix_numeric"), dpi=500)

    # print(correl)

def main():
    ## Load the datasets
    training_data = pd.read_csv(os.path.join(DATA_DIR, 'train.csv'))

    # distplot(training_data, variable='SalePrice', export=False)

    distplots(training_data, log_trans=True)

if __name__ == "__main__":
    main()
