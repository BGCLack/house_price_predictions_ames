## Dataframe and math libraries
import pandas as pd
import numpy as np
import scipy.stats as stats

## Plotting Libraries
import matplotlib.pyplot as plt

## System Libraries
import os
import pickle

## Scikit-Learn Libraries
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error, make_scorer
from sklearn.preprocessing import StandardScaler, RobustScaler, QuantileTransformer, LabelEncoder, OneHotEncoder
from sklearn.feature_selection import SelectKBest, f_regression
from sklearn.decomposition import PCA
from sklearn.linear_model import Ridge
from sklearn.model_selection import train_test_split, GridSearchCV, RandomizedSearchCV
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
import xgboost as xgb

## Preprocessing functions
import preprocessing as pp

## Scoring functions
from model_evaluation import simple_score, log_score


import warnings
warnings.filterwarnings('ignore')

## Project Directories
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DATA_DIR = os.path.join(BASE_DIR, 'Data')
OUTPUT_DIR = os.path.join(BASE_DIR, 'Output')
MODEL_DIR = os.path.join(OUTPUT_DIR, 'Models')

def save_model(model, model_name, cv_score, model_params):
    """
    Saves model to the model directory (creates one if it does not already exist).
    """
    ## Create model directory with title case if one does not exist.
    if not os.path.exists(os.path.join(MODEL_DIR, model_name.title())):
        os.makedirs(os.path.join(MODEL_DIR, model_name.title()))

    ## Save the model the in this directory with filename model_name.sav.
    model_file = open(os.path.join(os.path.join(MODEL_DIR, model_name.title()), model_name + '.sav'), 'wb')
    pickle.dump(model,  model_file)
    model_info = open(os.path.join(os.path.join(MODEL_DIR, model_name.title()), 'model_info.txt'), 'w')
    model_info.write('cv-score :' + cv_score + '\n\n')
    model_info.write('best parameters :' + model_params + '\n\n')
    model_info.write(str(model) + '\n\n')
    model_info.close()
    model_file.close()
    print("Model successfully saved.")

def load_model(model_name):
    """
    Loads the specified model.
    """
    model_location = os.path.join(os.path.join(MODEL_DIR, model_name.title()), model_name + '.sav')
    print(model_location)
    model_file= open(model_location, 'rb')
    model = pickle.load(model_file)
    model_file.close()
    print("Model successfully loaded.")

    return model

def create_submission(filename, preds_test, X_test):
    """
    Creates a submission file for the Kaggle competition.
    """
    submission = pd.DataFrame({'Id': X_test.index, 'SalePrice': preds_test})
    submission.to_csv(os.path.join(OUTPUT_DIR, filename), index=False)


def auto_pipline():
    """
    Pipeline to train House price predictor on training set. Uses Cross-validated approach Randomized grid search to optimize the hyperparamters.
    Transforms:       One-Hot Encoding
                      Standard Scaling
    Reduce Dimension: PCA
    Model:            XGBoost
    """
    ## Target feature
    target = 'SalePrice'

    ## Features to train on
    numeric_features = ['YearBuilt', 'OverallQual', 'OverallCond', 'LotArea',
    'BedroomAbvGr', 'FullBath', 'HalfBath', 'GarageCars', 'PoolArea', 'Fireplaces',
    'YearRemodAdd', 'GrLivArea', 'TotRmsAbvGrd', 'TotalBsmtSF', 'HasBsmt']
    object_features = ['CentralAir', 'LandContour', 'BldgType',
    'HouseStyle', 'ExterCond', 'Neighborhood']
    scale_features = ['YearBuilt', 'GrLivArea', 'TotRmsAbvGrd', 'PoolArea', 'LotArea', 'BedroomAbvGr']
    features = numeric_features + object_features

    ## Set scoring
    # custom_score = make_scorer(simple_score, greater_is_better=False)
    custom_score = make_scorer(log_score, greater_is_better=False)

    ## Load training set and split in train and val sets
    training_data, test_data = pp.data_prep(target)

    # training_data = pd.read_csv(os.path.join(DATA_DIR, 'train.csv'))
    # training_data.dropna(subset= [target], inplace=True)
    X_train, X_val, y_train, y_val = train_test_split(training_data[features], training_data[target], train_size=0.8, test_size=0.2)
    print("Training and validation tests loaded")

    ###
    ### Define the preprocessing transforms using ColumnTransformer
    ###
    transformer = ColumnTransformer(transformers=[
                                   ('scaler', StandardScaler(), scale_features),
                                   ('oh_encode', OneHotEncoder(handle_unknown='ignore'), object_features),
                                   ], remainder='passthrough')

    ###
    ### Set transform parameters
    ###
    n_features_to_test = np.arange(1, 20)
    one_to_left = stats.beta(10, 1)
    from_zero_positive = stats.expon(0, 50)

    params_list = []

    ## reduce_dim parameters (PCA)
    PCA_parameters = {'reduce_dim': [PCA()],
                    'reduce_dim__n_components': n_features_to_test,
                    }

    ## reduce_dim parameters (SelectKBest)
    Kbest_parameters = { 'reduce_dim': [SelectKBest(f_regression)],
                    'reduce_dim__k': n_features_to_test,
                    }

    ## XGBoost parameters
    XGB_params = {"XGboost__n_estimators": stats.randint(3, 40),
                "XGboost__max_depth": stats.randint(3, 40),
                "XGboost__learning_rate": stats.uniform(0.05, 0.4),
                "XGboost__colsample_bytree": one_to_left,
                "XGboost__subsample": one_to_left,
                "XGboost__gamma": stats.uniform(0, 10),
                'XGboost__reg_alpha': from_zero_positive,
                "XGboost__min_child_weight": from_zero_positive,
                }

    params_list.append(dict(**PCA_parameters, **XGB_params))
    params_list.append(dict(**Kbest_parameters, **XGB_params))

    # print(params_list)

    print("Pipeline parameters set")
    pipe = Pipeline([
        ('transform', transformer),
        ('reduce_dim', PCA()),
        ('XGboost', xgb.XGBRegressor(nthreads=-1, objective='reg:squarederror'))
        ])

    ###
    ### Default Parameter Pipe
    ###
    # pipe = pipe.fit(X_train, y_train)
    # print("pipeline fitted")
    # predictions = pipe.predict(X_val)
    # print(score_model(y_val, predictions))


    ###
    ### GridSearch
    ###
    # gridsearch = GridSearchCV(pipe, params, verbose=1, scoring=custom_score).fit(X_train, y_train)
    # print('Final score is: ', gridsearch.score(X_val, y_val))

    ###
    ### Random Grid Search
    ###
    ## Perform the CV grid-search on the entire training set
    randsearch = RandomizedSearchCV(pipe, params_list, verbose=1, scoring=custom_score, n_iter=10000, n_jobs=-1).fit(training_data[features], training_data[target])

    ## Print the best score from the search
    cv_score = randsearch.best_score_
    print('cv-score: ' + str(round(cv_score, 4)))

    ## Predict the validation set
    predictions = randsearch.predict(X_val)
    val_score = str(round(log_score(y_val, predictions), 4))
    model_params = str(randsearch.best_params_)

    ## Save the score and model
    print("Score on validation set: " + val_score)
    print("Best Parameters for XGBoost: " + model_params)
    # save_model(randsearch, 'Rand_Search_10000', val_score, model_params)

def testing():
    target = 'SalePrice'
    numeric_features = ['YearBuilt', 'OverallQual', 'OverallCond', 'LotArea',
    'BedroomAbvGr', 'FullBath', 'HalfBath', 'GarageCars', 'PoolArea', 'Fireplaces',
    'YearRemodAdd', 'GrLivArea', 'TotRmsAbvGrd', 'TotalBsmtSF', 'HasBsmt']
    object_features = ['CentralAir', 'LandContour', 'BldgType',
    'HouseStyle', 'ExterCond', 'Neighborhood']
    scale_features = ['YearBuilt', 'GrLivArea', 'TotRmsAbvGrd', 'PoolArea', 'LotArea', 'BedroomAbvGr']
    features = numeric_features + object_features

    training_data, test_data = pp.data_prep(target)
    print(test_data[features].isna().sum())
    test_data.loc[test_data['GarageCars']==np.nan,'GarageCars'] = 0



def main():
    auto_pipline()
    target = 'SalePrice'
    numeric_features = ['YearBuilt', 'OverallQual', 'OverallCond', 'LotArea',
    'BedroomAbvGr', 'FullBath', 'HalfBath', 'GarageCars', 'PoolArea', 'Fireplaces',
    'YearRemodAdd', 'GrLivArea', 'TotRmsAbvGrd', 'TotalBsmtSF', 'HasBsmt']
    object_features = ['CentralAir', 'LandContour', 'BldgType',
    'HouseStyle', 'ExterCond', 'Neighborhood']
    scale_features = ['YearBuilt', 'GrLivArea', 'TotRmsAbvGrd', 'PoolArea', 'LotArea', 'BedroomAbvGr']
    features = numeric_features + object_features

    training_data, test_data = pp.data_prep(target)
    model = load_model('Rand_Search_10000').best_estimator_
    test_preds = model.predict(test_data[features])
    final_predictions = np.exp(test_preds)
    print(final_predictions)

    create_submission('rand_search_10000.csv', final_predictions, test_data)

    # testing()


if __name__ == "__main__":
    main()
