import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns

import os
import pickle

import scipy.stats as stats

from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline

## Feature importance modules
import eli5
from eli5.sklearn import PermutationImportance
from pdpbox import pdp, get_dataset, info_plots
# import shap ## Not avaliable on python 3.8

import xgboost as xgb
import warnings
import preprocessing as pp

## Project Directories
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DATA_DIR = os.path.join(BASE_DIR, 'Data')
OUTPUT_DIR = os.path.join(BASE_DIR, 'Output')
MODEL_DIR = os.path.join(OUTPUT_DIR, 'Models')

def load_model(model_name):
    """
    Loads the specified model.
    """
    model_location = os.path.join(os.path.join(MODEL_DIR, model_name.title()), model_name + '.sav')
    print(model_location)
    model_file= open(model_location, 'rb')
    model = pickle.load(model_file)
    model_file.close()
    print("Model successfully loaded.")

    return model

def log_score(y_valid, predictions):
    return np.abs(np.sqrt(mean_squared_error(y_valid, predictions)))

def simple_score(y_valid, predictions):
    score = np.abs(np.sqrt(mean_squared_error(np.log(y_valid), np.log(predictions))))
    return score

def score_model(y_valid, predictions, log=False):
    """
    Scores the predictions against the known values for the validation set. The
    metric is the RMSE of the logarithms. If the target has been tranasformed the
    function will not apply the log for scoring. Otherwise it will.
    """
    if log == False:
        score = np.sqrt(mean_squared_error(np.log(y_valid), np.log(predictions)))
    elif log == True:
        score = np.sqrt(mean_squared_error(y_valid, predictions))
    return score

def perm_import(pipeline, features, X_val, y_val):
    perm = PermutationImportance(pipeline, random_state=1).fit(X_val, y_val)
    print(eli5.show_weights(perm, feature_names = features))
    # print(eli5.format_as_text(eli5.explain_weights(model)))

def part_plot_1D(model, total_features, X_val, y_val, feature):
    pdp_dist = pdp.pdp_isolate(model=model, dataset=X_val, model_features=total_features, feature=feature)
    pdp.pdp_plot(pdp_dist, feature)
    plt.show()

def part_plot_2D(model, total_features, X_val, y_val, feature1, feature2):
    inter1  =  pdp.pdp_interact(model=model, dataset=X_val, model_features=total_features, features=[feature1, feature2])
    pdp.pdp_interact_plot(pdp_interact_out=inter1, feature_names=[feature1, feature2], plot_type='grid')
    plt.show()

def shap_values(prediction, model):
    explainer = shap.TreeExplainer(model)
    shap_values = explainer.shap_values(prediction)
    shap.initjs()
    shap.force_plot(explainer.expected_value[0], shap_values[0], prediction)

def main():
    numeric_features = ['YearBuilt', 'OverallQual', 'OverallCond', 'LotArea',
    'BedroomAbvGr', 'FullBath', 'HalfBath', 'GarageCars', 'PoolArea', 'Fireplaces',
    'YearRemodAdd', 'GrLivArea', 'TotRmsAbvGrd', 'TotalBsmtSF', 'HasBsmt']
    object_features = ['CentralAir', 'LandContour', 'BldgType',
    'HouseStyle', 'ExterCond', 'Neighborhood']
    scale_features = ['YearBuilt', 'GrLivArea', 'TotRmsAbvGrd', 'PoolArea', 'LotArea', 'BedroomAbvGr']
    features = numeric_features + object_features
    ## Target feature (Predictor)
    target = 'SalePrice'

    ## Load training set and split in train and val sets
    training_data, test_data = pp.data_prep(target)
    X_train, X_val, y_train, y_val = train_test_split(training_data[features], training_data[target], train_size=0.8, test_size=0.2)

    pipeline = load_model('Rand_Search_1000').best_estimator_
    # print(model.best_estimator_.get_params())
    # X_val = Pipeline(pipeline.steps[:-1]).transform(X_val)
    print(X_val)
    print(Pipeline(pipeline.steps[:-1]))
    # print(X_val)
    # perm_import(pipeline, features, X_val, y_val)

if __name__ == '__main__':
    main()
